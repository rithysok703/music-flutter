<<<<<<< README.md
<div align="center"> <img src="assets/images/icon.png" height=200
width=200> </div>


# Music

Download and play songs from your phone. <br>

Go [here](#download-and-installation) for installation.

### Easily play and download songs

<div align="center"> <img width="49%" src="docs/images/phone_home.png">
<img width="49%" src="docs/images/phone_search.png"> </div>

<br>

### View all songs and by artists

<div align="center"> <img width="49%"
src="docs/images/phone_mymusic.png"> <img  width="49%"
src="docs/images/phone_artist.png"> </div>

<br>

### Automatically generated albums as well as Custom ones

<div align="center"> <img width="49%"
src="docs/images/phone_albums.png"> <img width="49%"
src="docs/images/phone_album.png"> </div>

<br>

### Shuffle and view queue

<div align="center"> <img width="49%" src="docs/images/phone_queue.png">
</div>

> This app is the mobile version of [this
> app](https://github.com/Lutetium-Vanadium/music), which is out of date
> and is soon to be updated.

If you have any issues or suggestions, feel free to [open a pull
request](https://github.com/Lutetium-Vanadium/music-flutter/pulls) or
[file an
issue](https://github.com/Lutetium-Vanadium/music-flutter/issues)

If you wish to customize or learn more about the project, go
[here](docs/codestructure.md).

## Download and Installation

For android, you can directly download a built version of the latest
release
[here](https://github.com/Lutetium-Vanadium/Music-Flutter/releases).
First generate the API Keys and then download the APK from the latest
release.

If you are on iOS or want to download and build the project yourself,
follow the steps given below.

### Api key

The app requires [A Napster API
Key](https://developer.napster.com/api/v2.2#getting-started), to
function.

Steps to create the API Key can be viewed [here](docs/apikeys.md).

Once you have those created, you can move onto running the app. You will
need to enter the api keys directly in the app.

### Build Dependecies

First [install flutter](https://flutter.dev/docs/get-started/install).
After that run:

```sh
flutter pub get
```

This will install dependecies.

### Testing

To run the tests, written for the app, run:

```sh
flutter test
```

### Running without a regular installation

If you wish to test the app to see if it works, connect a device or run
an emulator. To start the `profile` mode app (runs faster than `debug`
mode, but doesn't have the developer functionalities), run:

```sh
flutter run --profile --flavor dev
```

### Building

#### For android:

Signing the app:

- **Create a Keystore**

  If you have an existing keystore, skip to the next step. If not,
  create one by running the following at the command line:

  On Mac/Linux, use the following command:

  ```sh
  keytool -genkey -v -keystore ~/key.jks -keyalg RSA -keysize 2048
  -validity 10000 -alias key
  ```

  On Windows, use the following command:

  ```batch
  keytool -genkey -v -keystore c:\Users\USER_NAME\key.jks
  -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias key
  ```

  This command stores the `key.jks` file in your home directory. If you
  want to store it elsewhere, change the argument you pass to the
  `-keystore` parameter.

  > - The keytool command might not be in your path—it’s part of Java,
  >   which is installed as part of Android Studio. For the concrete
  >   path, run flutter doctor -v and locate the path printed after
  >   ‘Java binary at:’. Then use that fully qualified path replacing
  >   java (at the end) with keytool. If your path includes
  >   space-separated names, such as Program Files, use
  >   platform-appropriate notation for the names. For example, on
  >   Mac/Linux use Program\ Files, and on Windows use "Program Files".
  > - The -storetype JKS tag is only required for Java 9 or newer. As of
  >   the Java 9 release, the keystore type defaults to PKS12.

- **Reference the keystore from the app**

  Create a file named `android/key.properties` that contains a reference
  to your keystore:

  ```properties
  storePassword=<password from previous step>
  keyPassword=<password from previous step> keyAlias=key
  storeFile=<location of the key store file, such as /home/<user
  name>/key.jks>
  ```

Building APK:

- 'fat' APK

  ```sh
  flutter build apk --flavor prod
  ```

  This will build a 'fat' APK, which contains code compiled for all
  architectures, which means it can run on all android devices.

- Split APKs

  ```sh
  flutter build apk --split-per-abi --flavor prod
  ```

  This will split it into different APKs, based on the architecture
  which results in smaller app sizes, but it only works on phones with
  the specific architecture.

Once finished building, the APK will be available at
`build/app/outputs/flutter-apk/app-prod-release.apk`. To install, either
transfer the apk to your phone and install, or connect your device via
USB Tethering and run:

```sh
flutter install
```

You may be shown this popup when installing the app:

<img src="docs/images/play_protect.png" height="200">

You can safely click 'Install Anyway'.

#### For iOS

See flutter's [ios](https://flutter.dev/docs/deployment/ios) release
documentaion.

### IOS

The app was built with an android testing device and so it may not
function fully as intended in ios. In general the UI will look and work
the same, but platform specific things like notifications may not. For
example, android allows for progress notifications while ios does not.

=======
# music-flutter



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/rithysok703/music-flutter.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/rithysok703/music-flutter/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> README.md
